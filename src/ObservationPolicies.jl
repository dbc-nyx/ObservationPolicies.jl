module ObservationPolicies

using Observables

export CliqueObservable,
       onchange,
       commit, commit!,
       ObservationPolicy,
       observables, newobservable,
       IndependentObservables, TriggeredObservation, PeriodicObservation,
       Cooldown,
       Clock, tick, start!, stop!, restart!, isstarted

# ObservationPolicy
abstract type ObservationPolicy end

function observables end

function newobservable end

# CliqueObservable
abstract type AbstractObservable{T} <: Observables.AbstractObservable{T} end

struct CliqueObservable{T} <: AbstractObservable{T}
    input::Observables.AbstractObservable{T}
    output::Observable{T}
    transaction::Observable{Pair{T, T}}
    needs_update::Ref{Bool}
    needs_notify::Ref{Bool}
    policy::ObservationPolicy

    function CliqueObservable{T}(policy::ObservationPolicy,
                                 node::Observables.AbstractObservable{T},
                                ) where {T}
        val = node[]
        output = Observable(val)
        transaction = Observable(val => val)
        needs_update = Ref(false)
        needs_notify = Ref(false)
        on(node) do v
            needs_update[] = true
        end
        on(transaction) do (old, new)
            output.val = new
            needs_notify[] = true
            needs_update[] = false
        end
        new{T}(node, output, transaction, needs_update, needs_notify, policy)
    end
end

function notify′(obs::CliqueObservable)
    if obs.needs_notify[]
        obs.needs_notify[] = false
        notify(obs.output)
    end
end

notify′(clique::ObservationPolicy) = foreach(notify′, observables(clique))

#

input(obs) = obs
input(obs::CliqueObservable) = input(obs.input)

output(obs) = obs
output(obs::CliqueObservable) = output(obs.output)

Base.getindex(obs::AbstractObservable) = output(obs)[]
Base.setindex!(obs::AbstractObservable, val) = (input(obs)[] = val)
Base.notify(obs::AbstractObservable) = notify(input(obs))

Observables.observe(obs::AbstractObservable) = output(obs)

function commit(obs::AbstractObservable)
    if obs.needs_update[]
        prev = obs[]
        next = obs.input[]
        return prev => next
    else
        return nothing
    end
end

function commit!(obs::AbstractObservable)
    transaction = commit(obs)
    if !isnothing(transaction)
        obs.transaction[] = transaction
    end
    return transaction
end

haspolicy(::Observables.AbstractObservable) = false
haspolicy(::AbstractObservable) = true

#

observables(clique::ObservationPolicy) = clique.observables
listeners(clique::ObservationPolicy) = clique.listeners
eventloop(clique::ObservationPolicy) = clique.eventloop

# default implementation suitable only for TriggeredObservation and PeriodicObservation
function apply!(policy::ObservationPolicy)
    if isnothing(policy.eventloop)
        policy.eventloop = init!(policy; weak=true)
    end
end

function commit!(clique::ObservationPolicy)
    transaction = Vector{Pair{<:AbstractObservable{T}, Pair{T, T}} where {T}}()
    for obs in observables(clique)
        tra = commit!(obs)
        if !isnothing(tra)
            Base.push!(transaction, obs => tra)
        end
    end
    return transaction
end

function update!(clique::ObservationPolicy)
    transaction = commit!(clique)
    if isempty(transaction)
        false
    else
        for f in listeners(clique)
            f(transaction)
        end
        notify′(clique)
        true
    end
end

function update!(clique::ObservationPolicy, callback)
    transaction = commit!(clique)
    if isempty(transaction)
        false
    else
        callback(clique)
        for f in listeners(clique)
            f(transaction)
        end
        notify′(clique)
        true
    end
end

function newobservable(policy::ObservationPolicy, ::Type{T}, val) where {T}
    obs = CliqueObservable{T}(policy, val)
    Base.push!(policy.observables, obs)
    # apply! updates the observer functions to account for the new observable
    apply!(policy)
    return obs
end

newobservable(policy::ObservationPolicy, val::Observables.AbstractObservable{T},
    ) where {T} = newobservable(policy, T, val)

newobservable(policy::ObservationPolicy, val) = newobservable(policy, Observable(val))

function onchange(@nospecialize(f), clique::ObservationPolicy)
    push!(clique.listeners, f)
end

mutable struct IndependentObservables <: ObservationPolicy
    observables::Vector{AbstractObservable}
    listeners::Vector{Function}
    # onany returns a vector of observer functions
    eventloop::Vector{Observables.ObserverFunction}
end

IndependentObservables() = IndependentObservables(AbstractObservable[],
                                                  Function[],
                                                  Observables.ObserverFunction[])


function init!(clique::IndependentObservables; weak::Bool=false)
    # updates are immediately triggered on any `obs[] = val`
    # remember: `obs.needs_update` is not an observable
    onany(input.(observables(clique))...; weak=weak) do args...
        update!(clique)
    end
end

function apply!(policy::IndependentObservables)
    foreach(off, policy.eventloop)
    policy.eventloop = init!(policy; weak=true)
end

mutable struct TriggeredObservation <: ObservationPolicy
    observables::Vector{AbstractObservable}
    listeners::Vector{Function}
    eventloop::Union{Nothing, Observables.ObserverFunction}
    needs_update::Observable{Bool}
end

function TriggeredObservation()
    observables = AbstractObservable[]
    listeners = Function[]
    needs_update = Observable(false)
    TriggeredObservation(observables, listeners, nothing, needs_update)
end

function init!(clique::TriggeredObservation; weak::Bool=false)
    on(async_latest(clique.needs_update); weak=weak) do update
        if update
            update!(clique)
        end
    end
end

abstract type AbstractClock end

function tick end
function start! end
function stop! end
function restart! end
function isstarted end

struct Clock <: AbstractClock
    refreshrate::Observable{Float64}
    ticking::Observable{Bool}
    tick::Observable{Bool}

    function Clock(refreshrate::Observable{Float64},
                   ticking::Observable{Bool},
                   tick::Observable{Bool})
        function tick_repeat()
            while ticking[]
                sleep(0.5 / refreshrate[])
                # prepending `@async` causes `async_latest` listeners on `tick` to block
                tick[] = true
                sleep(0.5 / refreshrate[])
            end
        end
        on(ticking) do start
            if start
                @async tick_repeat()
            end
        end
        new(refreshrate, ticking, tick)
    end
end
Clock(refreshrate::Observable{Float64}) = Clock(refreshrate, Observable(false), Observable(false))
Clock(refreshrate::Float64) = Clock(Observable(refreshrate))

tick(clock::Clock) = clock.tick

function start!(clock::Clock)
    clock.ticking[] = true
    return clock
end

stop!(clock::Clock) = (clock.ticking[] = false)

function restart!(clock::Clock)
    stop!(clock)
    while clock.ticking[]
        sleep(0.01)
    end
    start!(clock)
end

isstarted(clock::Clock) = clock.ticking[]

mutable struct PeriodicObservation <: ObservationPolicy
    observables::Vector{AbstractObservable}
    listeners::Vector{Function}
    eventloop::Union{Nothing, Observables.ObserverFunction}
    clock::AbstractClock
end

function PeriodicObservation(clock::AbstractClock)
    observables = AbstractObservable[]
    listeners = Function[]
    PeriodicObservation(observables, listeners, nothing, clock)
end

function PeriodicObservation(refreshrate::Union{Float64, Observable{Float64}})
    PeriodicObservation(Clock(refreshrate))
end

function init!(clique::PeriodicObservation; weak::Bool=false)
    # TODO: find out why `async_latest` blocks here when `setindex!` on the `tick`
    #       observable is called using `@async`
    on(async_latest(tick(clique.clock)); weak=weak) do _
        update!(clique)
    end
end

function start!(clique::PeriodicObservation)
    start!(clique.clock)
    return clique
end

stop!(clique::PeriodicObservation) = stop!(clique.clock)

function restart!(clique::PeriodicObservation)
    restart!(clique.clock)
    return clique
end

isstarted(clique::PeriodicObservation) = isstarted(clique.clock)

function newobservable(policy::PeriodicObservation, ::Type{T}, val) where {T}
    if isempty(policy.observables) && !isstarted(policy.clock)
        start!(policy.clock)
    end
    obs = CliqueObservable{T}(policy, val)
    Base.push!(policy.observables, obs)
    apply!(policy)
    return obs
end

"""
    Timer(timeout)

Similar to Base.Timer, with an observable attribute `stop`.
"""
struct Timer
    timeout::Real
    ticking::Observables.AbstractObservable{Bool}
end

Timer(timeout) = Timer(timeout, Observable(false))

isstarted(timer::Timer) = timer.ticking[]

function start!(timer::Timer)
    if isstarted(timer)
        @debug "Timer is already counting down"
        return
    end
    timer.ticking[] = true
    @async begin
        sleep(timer.timeout)
        stop!(timer)
    end
end

function stop!(timer::Timer)
    isstarted(timer) || throw("timer is not counting down")
    timer.ticking[] = false
end

restart!(timer::Timer) = start!(timer)

tick(timer::Timer) = map(Base.:!, timer.ticking)

"""
    Cooldown(duration)

Observation policy that drops all but the last update for a fixed duration.

Basically, an observable with cooldown can either be in an activable or inactivated state.
In the initial activable state, an update is immediately processed and a timer is started.
The timer stops after the specified `duration` (in seconds).

While the timer is running, the observable is inactivated, and updates are dropped but the
last one. In addition, the last update is held until the observable is activable again, i.e.
after the timer stops.
"""
mutable struct Cooldown <: ObservationPolicy
    observables::Vector{AbstractObservable}
    listeners::Vector{Function}
    eventloop::Vector{Observables.ObserverFunction}
    timer::Timer
end

function Cooldown(timer::Timer)
    observables = AbstractObservable[]
    listeners = Function[]
    eventloop = Observables.ObserverFunction[]
    Cooldown(observables, listeners, eventloop, timer)
end

Cooldown(timeout::Real) = Cooldown(Timer(timeout))

activable(clique::Cooldown) = !isstarted(clique.timer)

function init!(clique::Cooldown; weak::Bool=false)
    eventloop = onany(input.(observables(clique))...; weak=weak) do args...
        activable(clique) && update!(clique, start!)
    end
    push!(eventloop, on(clique.timer.ticking; weak=weak) do ticking
              !ticking && update!(clique, restart!)
          end)
    return eventloop
end

function apply!(clique::Cooldown)
    foreach(off, clique.eventloop)
    clique.eventloop = init!(clique; weak=true)
end

function start!(clique::Cooldown)
    start!(clique.timer)
    return clique
end
function restart!(clique::Cooldown)
    restart!(clique.timer)
    return clique
end

end
